const User = require('../../models/user');
const bcrypt = require('bcrypt');
const helpers = require('../../helpers');
const { user } = require('./validator');

const create = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username })
        if (user) {
            return res.status(404).send({error: 'User already exists'})
        }
        const newUser = new User({
            username: req.body.username,
            password: await helpers.hash(req.body.password),
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString()
        })
        const result = await newUser.save();
        const token = helpers.generateJWT(result)
        return res.status(200).json({ result, token })
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

const get = async (req, res) => {
    try {
        const users = await User.find()
        return res.status(200).json({users})
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

const getOne = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.params.username })
        if (!user) {
            return res.status(404).send({error: 'Not found user'})
        }
        return res.status(200).json({user})
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

const deleteOne = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.params.username })
        if (!user) {
            return res.status(404).send({error: 'Not found user'})
        }
        user.delete()
        return res.status(200).send({message: 'User has been deleted'})
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

const update = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.params.username })
        if (!user) {
            return res.status(404).send({error: 'Not found user'})
        }
        if (!req.body.password) {
            return res.status(404).send({error: 'Password must not be empty'})
        }
        const newUser = await User.findByIdAndUpdate(user._id, {
            password: await helpers.hash(req.body.password),
            updatedAt: new Date().toISOString()
        }, {new: true})
        return res.status(200).send({newUser})
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
}

module.exports = {
    deleteOne,
    getOne,
    get,
    create,
    update
}