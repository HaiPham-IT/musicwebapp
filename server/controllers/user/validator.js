const Joi  = require('joi');

const user = Joi.object({
    username: Joi.string().alphanum().min(6).max(15).required(),
    password: Joi.string().alphanum().min(6).max(15).required(),
    passwordConfirm: Joi.string().valid(Joi.ref('password')).required(),
})

module.exports = {
    user
}
