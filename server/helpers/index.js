const config = require('../configs/app')
const jwt = require('jsonwebtoken');
const crypto = require("crypto")

function generateJWT(user){
    return jwt.sign({
        id: user.id,
        email: user.email,
        },
        config.secret, { expiresIn: '1h' })
}

async function hash(password) {
    return new Promise((resolve, reject) => {
        const salt = crypto.randomBytes(8).toString("hex")

        crypto.scrypt(password, salt, 64, (err, derivedKey) => {
            if (err) reject(err);
            resolve(salt + ":" + derivedKey.toString('hex'))
        });
    })
}

async function verify(password, hash) {
    return new Promise((resolve, reject) => {
        const [salt, key] = hash.split(":")
        crypto.scrypt(password, salt, 64, (err, derivedKey) => {
            if (err) reject(err);
            resolve(key == derivedKey.toString('hex'))
        });
    })
}

module.exports = {
    generateJWT,
    hash,
    verify
}