const mongoose = require('mongoose'),
      uniqueValidator = require('mongoose-unique-validator')

const schema = new mongoose.Schema({
    username: {type: String, index: true, required: true, unique: true, uniqueCaseInsensitive: false},
    password: {type: String, index: true, required: true},
    createdAt: { type: Date},
    updatedAt: {type: Date}
})

schema.plugin(uniqueValidator)

module.exports = mongoose.model('User', schema)