const express = require('express'),
      app = express(),
      config = require('./configs/app')
      morgan = require('morgan'),
      cors = require('cors')
require('./configs/db');
app.use(cors())    
app.use(express.json())    
app.use(express.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use(require('./routes'));
app.listen(config.port, () => {
    console.log(`Server is running at ${config.port}`)
})