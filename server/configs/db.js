const mongoose = require('mongoose');
const config = require('../configs/app')
mongoose.connect(config.mongodbUri, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
.then(() => {
    console.log('Successfully connected to mongo DB')
})
.catch(err => console.log(err))