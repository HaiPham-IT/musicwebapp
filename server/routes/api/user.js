const express = require('express');

const router = express.Router();

const user = require('../../controllers/user');

router.get('/', user.get);
router.get('/:username', user.getOne);
router.post('/', user.create);
router.put('/:username', user.update);
router.delete('/:username', user.deleteOne);

module.exports = router;
